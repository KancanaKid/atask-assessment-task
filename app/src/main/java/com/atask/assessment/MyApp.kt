package com.atask.assessment

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * @author basyi
 * Created 4/5/2023 at 10:29 AM
 */
@HiltAndroidApp
class MyApp : Application()