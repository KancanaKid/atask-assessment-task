package com.atask.assessment.ui.main

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.atask.assessment.data.local.database.MathExpression

/**
 * @author basyi
 * Created 4/6/2023 at 2:43 PM
 */

@Composable
fun DataItem(expression: MathExpression){
    Card(
        shape = RoundedCornerShape(4.dp),
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp)
    ) {
        Row(
            modifier = Modifier.fillMaxWidth().padding(8.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
        ) {
            Text(
                "Input : ${expression.expression}",
                modifier = Modifier
                    .height(32.dp)
                    .padding(2.dp),
                fontSize = 14.sp,
                fontWeight = FontWeight.SemiBold)

            Text(
                "Result : ${expression.result}",
                modifier = Modifier
                    .height(32.dp)
                    .padding(2.dp),
                fontSize = 14.sp,
                fontWeight = FontWeight.SemiBold)
        }
    }
}