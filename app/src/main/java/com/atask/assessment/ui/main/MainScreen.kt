package com.atask.assessment.ui.main

import android.Manifest
import android.annotation.SuppressLint
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.NavController
import com.atask.assessment.BuildConfig
import com.atask.assessment.R
import com.atask.assessment.data.local.database.MathExpression
import com.atask.assessment.ui.theme.White
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberMultiplePermissionsState

/**
 * @author basyi
 * Created 4/6/2023 at 10:19 AM
 */

@Composable
fun MainScreen(modifier: Modifier = Modifier, viewModel: MainViewModel = hiltViewModel(), navController: NavController){
    val lifecycle = LocalLifecycleOwner.current.lifecycle
    val items by produceState<UiState>(
        initialValue = UiState.Loading,
        key1 = lifecycle,
        key2 = viewModel
    ){
        lifecycle.repeatOnLifecycle(state = Lifecycle.State.STARTED){
            viewModel.uiState.collect{ value = it}
        }
    }

    CheckingPermission(items = items, navController = navController)
}

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
internal fun MainLayout(
    items:UiState,
    modifier: Modifier = Modifier,
    navController: NavController
){
    val scaffoldState: ScaffoldState = rememberScaffoldState()
    Scaffold(scaffoldState = scaffoldState,
        modifier = modifier.fillMaxSize(),
        floatingActionButton = {
            val isBuiltInCamera = BuildConfig.FLAVOR_type == "builtincamera"
            FloatingActionButton(onClick = {
                if(isBuiltInCamera){
                    navController.navigate("camera"){
                        popUpTo("main")
                    }
                }else{
                    navController.navigate("gallery")
                }
            },
                backgroundColor = MaterialTheme.colors.secondary,
                modifier = modifier.padding(start = 8.dp, end = 8.dp)
            ) {
                Icon( if(isBuiltInCamera) Icons.Default.Camera else Icons.Default.Image,
                    contentDescription = "",
                    modifier = Modifier.size(30.dp),
                )
            }
        },
        topBar = {
            TopAppBar(
                title = {
                    Text(text = stringResource(id = R.string.app_title), color = White)
                },
                backgroundColor = MaterialTheme.colors.primary,
                modifier = modifier.fillMaxWidth(),
                elevation = 8.dp,
                actions = {
                    IconButton(onClick = {
                        navController.navigate("browse")
                    }) {
                        Icon(
                            imageVector = Icons.Default.AttachFile,
                            contentDescription = "Menu icon"
                        )
                    }
                }
            )
        }
    ) {
        if(items is UiState.Success){
            val data = items.data
            if(data.isEmpty()) NoData()
            else Success(items = data)
        }else{
            Loading()
        }
    }
}

@Composable
internal fun Loading(){
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CircularProgressIndicator(
            color = MaterialTheme.colors.primary,
            modifier = Modifier.size(60.dp)
        )
        Text(text = stringResource(id = R.string.loading), style = MaterialTheme.typography.body1)
    }
}

@Composable
internal fun Success(
    items:List<MathExpression>
){
    val listState = rememberLazyListState()
    Column(
        modifier = Modifier.fillMaxWidth()
    ) {
        LazyColumn(state = listState){
            items(items){
                DataItem(expression = it)
            }
        }
    }
}

@Composable
internal fun NoData(){
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Icon(Icons.Default.Cancel,
            contentDescription = "",
            modifier = Modifier.size(75.dp),
        )
        Spacer(modifier = Modifier.height(16.dp))
        Text(text = stringResource(id = R.string.no_data), style = MaterialTheme.typography.body2)
    }
}

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@OptIn(ExperimentalPermissionsApi::class)
@Composable
internal fun CheckingPermission(items:UiState, navController: NavController){
    val permissionState = rememberMultiplePermissionsState(
        permissions = listOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
    )
    val lifecycleOwner = LocalLifecycleOwner.current
    DisposableEffect(key1 = lifecycleOwner, effect = {
        val observer = LifecycleEventObserver{_, event ->
            when(event){
                Lifecycle.Event.ON_START ->{
                    permissionState.launchMultiplePermissionRequest()
                }
                else -> {}
            }
        }
        lifecycleOwner.lifecycle.addObserver(observer)
        onDispose {
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    })

    if(permissionState.allPermissionsGranted){
        MainLayout(items = items, navController = navController)
    }else{
        ShowPermissionNotGrantedUi()
    }
}
