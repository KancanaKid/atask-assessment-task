package com.atask.assessment.ui.theme

import androidx.compose.ui.graphics.Color

val Red500 = Color(0xFFF44336)
val Red700 = Color(0xFFD32F2F)
val Green500 = Color(0xFF4CAF50)
val Green700 = Color(0xFF388E3C)
val Amber200 = Color(0xFFFFE082)
val Grey = Color(0xFFEEEEEE)
val White = Color(0xFFFFFFFF)