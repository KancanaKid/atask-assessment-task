package com.atask.assessment.ui.browse

import android.annotation.SuppressLint
import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.atask.assessment.R
import com.atask.assessment.ui.main.MainViewModel
import com.atask.assessment.ui.theme.White
import com.atask.assessment.ui.utils.fileFromContentUri
import kotlinx.coroutines.launch
import java.io.*

/**
 * @author basyi
 * Created 4/10/2023 at 8:42 PM
 */

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun BrowseFileScreen(modifier: Modifier = Modifier,  navController: NavController, viewModel : MainViewModel = hiltViewModel()){
    val scaffoldState: ScaffoldState = rememberScaffoldState()
    val scope = rememberCoroutineScope()
    val context = LocalContext.current

    var fileUri by remember {
        mutableStateOf<Uri?>(Uri.EMPTY)
    }

    var decodeResult by remember {
        mutableStateOf("")
    }

    val fileBrowseLauncher = rememberLauncherForActivityResult(ActivityResultContracts.OpenDocument()){
        fileUri = it
        if(fileUri != Uri.EMPTY){
            fileUri?.let {uri ->
                val file = fileFromContentUri(context, fileUri!!)
                try{
                    file.forEachLine {
                        viewModel.decodeFile(it)
                    }
                }catch (e:IOException){
                    e.printStackTrace()
                }

            }

        }
    }

    Scaffold(scaffoldState = scaffoldState,
        modifier = modifier.fillMaxSize(),
        topBar = {
            TopAppBar(
                title = {
                    Text(text = stringResource(id = R.string.browse_file), color = White)
                },
                backgroundColor = MaterialTheme.colors.primary,
                modifier = modifier.fillMaxWidth(),
                navigationIcon = {
                    IconButton(onClick = { navController.popBackStack() }) {
                        Icon(Icons.Filled.ArrowBack, contentDescription = "Back button")
                    }
                },
                elevation = 8.dp
            )
        }) {

        viewModel.decodeLiveData.observe(LocalLifecycleOwner.current){
            decodeResult = it
        }

        Column(modifier = modifier
            .fillMaxWidth()
            .padding(16.dp)) {
            Text(text = "Result : $decodeResult", style = MaterialTheme.typography.h6, modifier = modifier.align(
                Alignment.CenterHorizontally))
            Spacer(modifier = modifier.weight(1f))
            Button(modifier = modifier.fillMaxWidth(), onClick = {
                scope.launch {
                    fileBrowseLauncher.launch(arrayOf("text/plain"))
                }
            }) {
                Text(text = "Browse File")
            }
        }
    }
}