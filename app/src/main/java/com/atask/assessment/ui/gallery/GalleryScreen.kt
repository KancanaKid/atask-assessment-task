package com.atask.assessment.ui.gallery

import android.annotation.SuppressLint
import android.net.Uri
import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.atask.assessment.R
import com.atask.assessment.ui.main.MainViewModel
import com.atask.assessment.ui.theme.White
import com.atask.assessment.ui.utils.Operation
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import kotlinx.coroutines.launch
import java.io.IOException
import androidx.compose.runtime.livedata.observeAsState
import com.atask.assessment.ui.theme.Grey
import com.atask.assessment.ui.utils.DialogBoxLoading

/**
 * @author basyi
 * Created 4/8/2023 at 3:30 PM
 */

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun GalleryScreen(modifier: Modifier = Modifier, navController: NavController, viewModel: MainViewModel = hiltViewModel()){
    var hasImage by remember {
        mutableStateOf(false)
    }

    var imageUri by remember {
        mutableStateOf<Uri?>(null)
    }

    var result by remember {
        mutableStateOf("")
    }

    var expression by remember {
        mutableStateOf("")
    }

    var imagePicker = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.GetContent(),
        onResult = { uri ->
            hasImage = uri != null
            imageUri = uri
        }
    )

    val context = LocalContext.current
    val recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)
    val scaffoldState: ScaffoldState = rememberScaffoldState()
    val scope = rememberCoroutineScope()
    val dialogOpen by viewModel.dialogOpen.observeAsState(false)

    Scaffold(scaffoldState = scaffoldState,
        modifier = modifier.fillMaxSize(),
        topBar = {
            TopAppBar(
                title = {
                    Text(text = stringResource(id = R.string.process_from_gallery), color = White)
                },
                backgroundColor = MaterialTheme.colors.primary,
                modifier = modifier.fillMaxWidth(),
                navigationIcon = {
                    IconButton(onClick = { navController.popBackStack() }) {
                        Icon(Icons.Filled.ArrowBack, contentDescription = "Back button")
                    }
                },
                elevation = 8.dp
            )
        }
    ) {
        viewModel.insertLiveData.observe(LocalLifecycleOwner.current){
            if(it){
                navController.popBackStack()
            }else{
                // show failed message
                scope.launch {
                    scaffoldState.snackbarHostState.showSnackbar("Saving result is failed", duration = SnackbarDuration.Short)
                }
            }
        }

        if(dialogOpen){
            DialogBoxLoading()
        }

        Column(modifier = modifier
            .padding(16.dp)) {
            Box(modifier = modifier
                .fillMaxWidth()
                .height(400.dp)
                .padding(8.dp).background(color = Grey, shape = RoundedCornerShape(4.dp)),

            ) {
                if(hasImage && imageUri != null) {
                    AsyncImage(
                        model = imageUri,
                        modifier = modifier
                            .fillMaxSize(),
                        placeholder = painterResource(id = R.drawable.ic_launcher_background),
                        contentDescription = "Selected image")
                }
            }
            Row(modifier = modifier.fillMaxWidth()) {
                Button(modifier = modifier
                    .weight(1f)
                    .padding(end = 5.dp),onClick = { imagePicker.launch("image/*") }) {
                    Text(text = "Select Image")
                }
                Button(modifier = modifier
                    .weight(1f)
                    .padding(start = 5.dp),  onClick = {
                    if(imageUri == null) {
                        result = "Invalid inputs"
                        return@Button
                    }
                    viewModel.dialogOpen.value = true
                    var image: InputImage? = null
                    try {
                        imageUri?.let {
                            image = InputImage.fromFilePath(context, it)
                            image?.let {inputImage ->
                                val result = recognizer.process(inputImage).addOnSuccessListener {visionText ->
                                    val blockText = visionText.text

                                    var arity1: Int
                                    var arity2: Int
                                    try {
                                        if(blockText.length > 3){
                                            arity1 = blockText.substring(0,1).toInt()
                                            arity2 = blockText.substring(2,3).toInt()
                                        }else{
                                            arity1 = blockText.substring(0,1).toInt()
                                            arity2 = blockText.substring(2).toInt()
                                        }
                                        val operand = blockText.substring(1,2)
                                        Operation(arity1, arity2, operand) {resultOperand ->
                                            if(resultOperand != "Invalid"){
                                                result = resultOperand
                                                expression = "$arity1 $operand $arity2"
                                            }else{
                                                result = "Invalid operand"
                                            }
                                        }
                                        viewModel.dialogOpen.value = false
                                    }catch (e:Exception){
                                        result = "Invalid inputs"
                                        viewModel.dialogOpen.value = false
                                    }
                                }.addOnFailureListener {
                                    result = "Invalid inputs"
                                    viewModel.dialogOpen.value = false
                                }
                            }
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                        result = "Invalid inputs"
                        viewModel.dialogOpen.value = false
                    }
                }) {
                    Text(text ="Process Result")
                }
            }
            Spacer(modifier = modifier
                .fillMaxWidth()
                .height(16.dp))
            Text(text = "Result : $result", style = MaterialTheme.typography.h6, modifier = modifier.align(Alignment.CenterHorizontally))
            Spacer(modifier = modifier.weight(1f))
            Button(modifier = modifier.fillMaxWidth(), onClick = {
                if(result.isNotEmpty()) viewModel.add(expression, result)
                else {
                    scope.launch {
                        scaffoldState.snackbarHostState.showSnackbar(message = "The result is empty", duration = SnackbarDuration.Short)
                    }
                }
            }) {
                Text(text = "Save Result")
            }
        }
    }
    

}
