package com.atask.assessment.ui.utils

/**
 * @author basyi
 * Created 4/9/2023 at 10:59 AM
 */

fun Operation(arity1:Int, arity2: Int, operand:String, onResult: (String) -> Unit){
    val result:Int
    when(operand){
        "+" -> {
            result = arity1 + arity2
            onResult.invoke(result.toString())
        }
        "-" -> {
            result =  arity1 - arity2
            onResult.invoke(result.toString())
        }
        "*" -> {
            result =  arity1 * arity2
            onResult.invoke(result.toString())
        }
        "X" -> {
            result =  arity1 * arity2
            onResult.invoke(result.toString())
        }
        "/" -> {
            result =  arity1 / arity2
            onResult.invoke(result.toString())
        }
        ":" -> {
            result =  arity1 / arity2
            onResult.invoke(result.toString())
        }
        else ->{
            onResult.invoke("Invalid")
        }
    }
}