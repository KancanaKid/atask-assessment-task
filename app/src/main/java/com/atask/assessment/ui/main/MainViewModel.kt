package com.atask.assessment.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.atask.assessment.data.local.database.MathExpression
import com.atask.assessment.data.repository.IMainRepository
import com.atask.assessment.ui.main.UiState.Success
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author basyi
 * Created 4/6/2023 at 10:20 AM
 */

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: IMainRepository
):ViewModel(){
    val uiState : StateFlow<UiState> = repository.mathExpressions
        .map<List<MathExpression>, UiState>(::Success)
        .catch { emit(UiState.Error(it)) }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), UiState.Loading)

    val insertLiveData by lazy { MutableLiveData<Boolean>() }
    val decodeLiveData by lazy { MutableLiveData<String>() }
    var dialogOpen = MutableLiveData<Boolean>()

    fun add(expression:String, result:String){
        viewModelScope.launch {
            repository.add(expression, result, onSuccess = {result ->
                insertLiveData.postValue(result.isNotEmpty())
            }, onError = {
                insertLiveData.postValue(false)
            })
        }
    }

    fun decodeFile(data:String) {
        viewModelScope.launch {
            repository.decodeData(data, onSuccess = {
                decodeLiveData.postValue(it)
            }, onError = {error ->
                decodeLiveData.postValue(error)
            })
        }
    }
}

sealed interface UiState{
    object Loading : UiState
    data class Error(val throwable: Throwable):UiState
    data class Success(val data:List<MathExpression>):UiState
}