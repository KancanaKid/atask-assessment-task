package com.atask.assessment.ui.main

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.atask.assessment.R
import com.atask.assessment.ui.theme.Red500
import com.atask.assessment.ui.theme.White

/**
 * @author basyi
 * Created 4/8/2023 at 1:15 PM
 */

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun ShowPermissionNotGrantedUi(){
    val scaffoldState: ScaffoldState = rememberScaffoldState()
    Scaffold(scaffoldState = scaffoldState,
        modifier = Modifier.fillMaxSize(),
        topBar = {
            TopAppBar(
                title = {
                    Text(text = stringResource(id = R.string.app_title), color = White)
                },
                backgroundColor = MaterialTheme.colors.primary,
                modifier = Modifier.fillMaxWidth()
            )
        }
    ) {
        Column(modifier = Modifier.fillMaxSize().padding(16.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(text = "Information",
                style = MaterialTheme.typography.h5,
                color = Red500
            )
            Text(text = "This app need permissions, you can enable it on app setting.",
                style = MaterialTheme.typography.subtitle2
            )
        }
    }
}