package com.atask.assessment.ui

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.atask.assessment.ui.browse.BrowseFileScreen
import com.atask.assessment.ui.camera.CameraScreen
import com.atask.assessment.ui.gallery.GalleryScreen
import com.atask.assessment.ui.main.MainScreen
import com.atask.assessment.ui.splash.SplashScreen
import com.google.accompanist.systemuicontroller.rememberSystemUiController


/**
 * @author basyi
 * Created 4/6/2023 at 10:15 AM
 */

@Composable
fun MainNavigation(){
    val navController = rememberNavController()
    val systemUiController = rememberSystemUiController()
    val useDarkIcons = !isSystemInDarkTheme()
    val darkColor = MaterialTheme.colors.primaryVariant

    SideEffect {
        systemUiController.setSystemBarsColor(
            color = darkColor,
            darkIcons = useDarkIcons
        )
    }
    NavHost(navController = navController, startDestination = "splash"){
        composable("splash"){
            SplashScreen(navController = navController)
        }
        composable("main"){
            MainScreen(navController = navController)
        }
        composable("camera"){
            CameraScreen(navController = navController)
        }
        composable("gallery"){
            GalleryScreen(navController = navController)
        }
        composable("browse"){
            BrowseFileScreen(navController = navController)
        }
    }
}
