package com.atask.assessment.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import com.atask.assessment.BuildConfig

private val DarkColorPalette = darkColors(
    primary = if (BuildConfig.FLAVOR_mode == "red") Red500 else Green500,
    primaryVariant = if (BuildConfig.FLAVOR_mode == "red") Red700 else Green700,
    secondary = Amber200
)

private val LightColorPalette = lightColors(
    primary = if (BuildConfig.FLAVOR_mode == "red") Red500 else Green500,
    primaryVariant = if (BuildConfig.FLAVOR_mode == "red") Red700 else Green700,
    secondary = Amber200

    /* Other default colors to override
    background = Color.White,
    surface = Color.White,
    onPrimary = Color.White,
    onSecondary = Color.Black,
    onBackground = Color.Black,
    onSurface = Color.Black,
    */
)

@Composable
fun AtaskAssessmentTestTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}