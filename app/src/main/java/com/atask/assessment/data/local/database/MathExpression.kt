package com.atask.assessment.data.local.database

import androidx.room.*
import kotlinx.coroutines.flow.Flow

/**
 * @author basyi
 * Created 4/6/2023 at 12:00 PM
 */
@Entity
data class MathExpression(
    val expression:String,
    val result:String
){
    @PrimaryKey(autoGenerate = true)
    var uid:Int = 0
}

@Dao
interface MathExpressionDao{
    @Query("SELECT * FROM mathexpression ORDER BY uid DESC")
    fun getAll() : Flow<List<MathExpression>>

    @Insert
    suspend fun insert(item:MathExpression)
}
