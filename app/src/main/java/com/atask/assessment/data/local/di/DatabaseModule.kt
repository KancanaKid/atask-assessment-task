package com.atask.assessment.data.local.di

import android.content.Context
import androidx.room.Room
import com.atask.assessment.data.local.database.AppDatabase
import com.atask.assessment.data.local.database.MathExpressionDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * @author basyi
 * Created 4/6/2023 at 12:08 PM
 */
@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext context: Context):AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java,"AssessmentTest")
            .fallbackToDestructiveMigration().build()
    }

    @Provides
    @Singleton
    fun provideMathExpressionDao(appDatabase: AppDatabase) : MathExpressionDao{
        return appDatabase.MathExpressionDao()
    }
}