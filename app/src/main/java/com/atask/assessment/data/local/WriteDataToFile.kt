package com.atask.assessment.data.local

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Environment
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * @author basyi
 * Created 4/9/2023 at 12:43 PM
 * The encrypted file would be saved on public Download directory
 */

@SuppressLint("SimpleDateFormat")
fun writeDataToFile(data:String){
    var fileOutputStream:FileOutputStream? = null
    val folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
    val time = Calendar.getInstance().time
    val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm")
    val current = formatter.format(time)
    val content = base64Encode(data)
    val file = File(folder, "${current}.txt")
    try {
        fileOutputStream = FileOutputStream(file)
        fileOutputStream.write(content.toByteArray())
    }catch (e:Exception){
        e.printStackTrace()
    }finally {
        if(fileOutputStream != null){
            try {
                fileOutputStream.close()
            }catch (e:IOException){
                e.printStackTrace()
            }
        }
    }
}