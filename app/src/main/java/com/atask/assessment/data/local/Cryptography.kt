package com.atask.assessment.data.local

/**
 * @author basyi
 * Created 4/11/2023 at 8:19 AM
 */

fun base64Encode(data: String):String{
    return android.util.Base64.encodeToString(data.toByteArray(), android.util.Base64.DEFAULT)
}

fun base64Decode(data: String):String{
    return String(android.util.Base64.decode(data, android.util.Base64.DEFAULT))
}