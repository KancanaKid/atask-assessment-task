package com.atask.assessment.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase

/**
 * @author basyi
 * Created 4/6/2023 at 12:06 PM
 */
@Database(entities = [MathExpression::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun MathExpressionDao():MathExpressionDao
}