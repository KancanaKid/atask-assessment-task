package com.atask.assessment.data.local.di

import android.content.Context
import com.atask.assessment.data.local.database.MathExpressionDao
import com.atask.assessment.data.repository.IMainRepository
import com.atask.assessment.data.repository.MainRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ViewModelScoped

/**
 * @author basyi
 * Created 4/6/2023 at 12:20 PM
 */
@Module
@InstallIn(ViewModelComponent::class)
object RepositoryModule {

    @Provides
    @ViewModelScoped
    fun provideMainRepository(mathExpressionDao: MathExpressionDao, @ApplicationContext context: Context) : IMainRepository {
        return MainRepository(mathExpressionDao, context)
    }
}