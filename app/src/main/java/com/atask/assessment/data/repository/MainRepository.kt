package com.atask.assessment.data.repository

import android.content.Context
import com.atask.assessment.data.local.base64Decode
import com.atask.assessment.data.local.writeDataToFile
import com.atask.assessment.data.local.database.MathExpression
import com.atask.assessment.data.local.database.MathExpressionDao
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * @author basyi
 * Created 4/6/2023 at 12:15 PM
 */
class MainRepository @Inject constructor(
    private val mathExpressionDao: MathExpressionDao,
    private val context: Context
) : IMainRepository {
    override val mathExpressions: Flow<List<MathExpression>>
        get() = mathExpressionDao.getAll()

    override suspend fun add(expression: String, result: String, onSuccess: (String) -> Unit, onError: (String) -> Unit) {
        try {
            mathExpressionDao.insert(MathExpression(expression, result))
            val data = "$expression = $result"
            writeDataToFile(data)
            onSuccess(data)
        }catch (e:Exception){
            onError(e.message!!)
        }
    }

    override suspend fun decodeData(data: String, onSuccess: (String) -> Unit, onError: (String) -> Unit) {
        try {
            onSuccess(base64Decode(data))
        }catch (e:Exception){
            onError(e.message!!)
        }
    }

}

interface IMainRepository{
    val mathExpressions:Flow<List<MathExpression>>
    suspend fun add(expression:String, result:String, onSuccess : (String) -> Unit, onError: (String) -> Unit)
    suspend fun decodeData(data:String, onSuccess:(String) -> Unit, onError: (String) -> Unit)
}