package com.atask.assessment

import android.util.Log
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.atask.assessment.data.local.*

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.atask.assessment", appContext.packageName)
    }

    @Test
    fun encryptTest() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        val data = "1+1=2"
        val encode = base64Encode(data)
        //MSsxPTI=
        Log.i("encode", encode.toByteArray().toString())
        Log.i("decode", base64Decode("MSArIDEgPSAy"))
    }
}